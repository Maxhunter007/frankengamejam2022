using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] string layerName;

    [HideInInspector] public int Damage;

    private AudioSource sfxAudioSource;
    [SerializeField]
    private AudioClip hitClip;
    [SerializeField, Range(0, 1)]
    private float hitVolume = 0.5f;
    [SerializeField]
    private AudioClip shotClip;
    [SerializeField, Range(0, 1)]
    private float shotVolume = 0.3f;



    public void OnEnable()
    {
        sfxAudioSource = GameObject.FindGameObjectWithTag("SFXAudioSource").GetComponent<AudioSource>();
        sfxAudioSource.PlayOneShot(shotClip, shotVolume);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.rigidbody != null && collision.rigidbody.gameObject.layer == LayerMask.NameToLayer(layerName))
        {
            collision.rigidbody.gameObject.GetComponentInChildren<UnitController>().TakeDamage(Damage);
            sfxAudioSource.PlayOneShot(hitClip, hitVolume);
        }
        GameObject.Destroy(this.gameObject);
    }

   
}
