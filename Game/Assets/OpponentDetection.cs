using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OpponentDetection : MonoBehaviour
{
    [SerializeField]
    private Transform transformToRotate;
    [SerializeField]
    private Movement movement;


    [SerializeField]
    private UnitController unityController;

    private bool isShooting = false;

    private List<Transform> activeEnemies = new List<Transform>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        activeEnemies.Add(collision.transform);
        if (isShooting)
        {
            return;
        }
        isShooting = true;
        unityController.StartShooting();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        activeEnemies.Remove(collision.transform);
        StopShooting();

    }

    private void StopShooting()
    {
        if (isShooting && activeEnemies.Count == 0)
        {
            movement.StartMoving();
            isShooting = false;
            unityController.StopShooting();
        }
    }

    void FixedUpdate()
    {
        for (int i = activeEnemies.Count - 1; i >= 0; i--)
        {
            if (activeEnemies[i] == null)
            {
                activeEnemies.RemoveAt(i);
            }
        }
        if(activeEnemies.Count > 0)
        {
            movement.Stop();
            var oldRotation = transformToRotate.rotation;
            transformToRotate.LookAt(activeEnemies
                .OrderBy(x => Vector3.Distance(this.transformToRotate.position, x.position))
                .First().transform.position);
            transformToRotate.Rotate(0, -90f, 0);
            
            transformToRotate.rotation = Quaternion.Lerp(oldRotation, transformToRotate.rotation,0.5f);
        }
        else if(isShooting)
        {
            StopShooting();
        }
    }
}
