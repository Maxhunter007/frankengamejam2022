﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.AI;


public class UnitSpawner : MonoBehaviour
{
    //Du musst dir 
    //Das Player Script suchen (wird irgendwo gemacht)
    //Check ob maus gedrück ist siehe Movement Script (in update method)
    //Du musst ein Prefab instantieren (siehe bullet spawning in UnitController
    //Du musst dem die Position der Maus geben 

    [SerializeField] GameObject Gunship;
    [SerializeField] Transform unitSpawnPosition;
    [SerializeField] Transform targetPosition;
    [SerializeField] GameObject HoverInfos;
    [SerializeField] TextMeshProUGUI hptext;
    [SerializeField] TextMeshProUGUI damagetext;
    [SerializeField] TextMeshProUGUI speedtext;
    [SerializeField] TextMeshProUGUI armortext;
    [SerializeField] TextMeshProUGUI moneytext;
    private Player player;


    public void onButtonClick()
    {


        if (player.Money >= Gunship.GetComponent<UnitController>().Credits)
        {
            player.Money = player.Money - Gunship.GetComponent<UnitController>().Credits;
            player.UpdateUI();
            GameObject GunshipInstance = GameObject.Instantiate(Gunship);
            GunshipInstance.SetActive(false);
            GunshipInstance.transform.position = this.unitSpawnPosition.position;
            GunshipInstance.SetActive(true);
            GunshipInstance.GetComponent<NavMeshAgent>().SetDestination(targetPosition.position);
        }



    }

    private void Start()
    {
        player = GameObject.FindObjectOfType<Player>();
    }

    public void OnHoverEnter()
    {
        HoverInfos.SetActive(true);
        hptext.text = Gunship.GetComponent<UnitController>().health.ToString();
        damagetext.text = Gunship.GetComponent<UnitController>().damage.ToString();
        speedtext.text = Gunship.GetComponent<NavMeshAgent>().speed.ToString();
        armortext.text = Gunship.GetComponent<UnitController>().armor.ToString();
        moneytext.text = Gunship.GetComponent<UnitController>().Credits.ToString();
    }

    public void OnHoverExit()
    {
        HoverInfos.SetActive(false);
    }
}