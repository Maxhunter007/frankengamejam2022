using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mine : MonoBehaviour
{
    [SerializeField]
    private int goldPerUpdate;

    [SerializeField]
    private int health;

    [SerializeField]
    private RectTransform lifeBar;


    [SerializeField]
    private float updateTime;

    private Player player;

    private float initialWidth;
    private float initialHealth;

    //Wemm die zahl im minus ist kontrollieren die Gegner die Mine ansonsten wir (0 gleichstand)
    private int mineControllingBalance;
    public void AddUnit(bool isPlayerUnit)
    {
        if (isPlayerUnit)
        {
            mineControllingBalance++;
        }
        else
        {
            mineControllingBalance--;
        }
    }

    public void RemoveUnit(bool isPlayerUnit)
    {
        if (isPlayerUnit)
        {
            mineControllingBalance--;
        }
        else
        {
            mineControllingBalance++;
        }
    }

    public void Start()
    {
        initialHealth = health;
        player = GameObject.FindObjectOfType<Player>();
        initialWidth = lifeBar.sizeDelta.x;// lifeBar.rect.width;
        StartCoroutine(MineUpdateRate());
    }

    public IEnumerator MineUpdateRate()
    {
        while (this.isActiveAndEnabled)
        {
            yield return new WaitForSeconds(updateTime);
            if(mineControllingBalance > 0)
            {
                player.Money = player.Money + goldPerUpdate;
                player.UpdateUI();
            }
            else if(mineControllingBalance < 0)
            {
                health--;
                if(health <= 0)
                {
                    if(GameObject.FindObjectsOfType<Mine>().Length == 1)
                    {
                        SceneManager.LoadScene(2);
                    }
                    GameObject.Destroy(this.gameObject);
                }
                var ratio = Mathf.InverseLerp(0, initialHealth, health);
                lifeBar.sizeDelta = new Vector2( Mathf.Lerp(0, initialWidth, ratio), lifeBar.sizeDelta.y);
            }
        }
    }



    
}
