using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

[Serializable]
public class WaveConfiguration
{
	[field: SerializeField] public List<LaneConfiguration> Lanes { get; private set; }

    [field: SerializeField] public float WaveWaitTime { get; private set; }
}

[Serializable]
public class LaneConfiguration
{
    [field: SerializeField] public Transform TargetMinePosition { get; private set; }
	[field: SerializeField] public List<EnemyConfiguration> Enemies { get; private set; }
}

[Serializable]
public class EnemyConfiguration
{
    [field: SerializeField] public GameObject EnemyPrefab { get; private set; }
    [field: SerializeField] public int NumberOfEnemies { get; private set; }
}

public class EnemySpawner : MonoBehaviour
{
	[SerializeField]
	private List<Transform> laneSpawnPositions;

	[SerializeField]
	private List<WaveConfiguration> waveConfigurations;

	private WaveConfiguration currentWave;

    private void Start()
	{
		NextWave();

    }

	private void NextWave()
	{
		var newWaveIndex = waveConfigurations.IndexOf(currentWave) + 1;
		if(newWaveIndex >= waveConfigurations.Count)
		{
			SceneManager.LoadScene(1);
			return;
		}
        currentWave = waveConfigurations[newWaveIndex];
		StartCoroutine(SpawnCurrentWave());
    }

	private IEnumerator SpawnCurrentWave()
	{
		yield return new WaitForSeconds(currentWave.WaveWaitTime);

		for(int i = 0; i < currentWave.Lanes.Count; i++)
		{
			foreach(var enemy in currentWave.Lanes[i].Enemies)
			{
				for(int x = 0; x < enemy.NumberOfEnemies; x++)
				{
					var enemyInstance = GameObject.Instantiate(enemy.EnemyPrefab);
					enemyInstance.SetActive(false);
                    enemyInstance.transform.position = laneSpawnPositions[i].position;
                    enemyInstance.SetActive(true);
					enemyInstance.GetComponent<NavMeshAgent>().SetDestination(currentWave.Lanes[i].TargetMinePosition.position);


				}
			}
		}
		NextWave();

    }

}
