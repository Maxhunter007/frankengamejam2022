using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Tilemaps;

public class Movement : MonoBehaviour
{
    private NavMeshAgent agent;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rigidbody2D;
    [SerializeField]
    private AudioSource dirvingSound;
    private bool updateRotation = true;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        rigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        dirvingSound.Play();
    }
    void Update()
    {
        if (updateRotation)
        {
            Vector3 lookDirection = agent.velocity.normalized;
            transform.rotation = Quaternion.LookRotation(Vector3.back, lookDirection);
            transform.Rotate(Vector3.forward, 90f);
        }
    }
    public void Stop()
    {
        updateRotation = false;
        if (dirvingSound.isPlaying)
        {
            dirvingSound.Stop();
        }
    }

    public void StartMoving()
    {
        updateRotation = true;

        if (!dirvingSound.isPlaying)
        {
            dirvingSound.Play();
        }
    }
    
}

