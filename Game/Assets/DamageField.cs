using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DamageField : MonoBehaviour
{
    [SerializeField] private int damage = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Unit"))
        {
            other.gameObject.GetComponent<UnitController>().TakeDamage(damage);
        }
    }
}
