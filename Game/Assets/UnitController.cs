using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class UnitController : MonoBehaviour
{
    // string = text
    // int = ganz zahl 1 2 3 4 5 -1 
    // float = komma zahl 1.0 1.091 ... 
    // bool = wahr oder falsch // true false
    //GameObject = Unity Object (wie im editor

    [SerializeField] public int health;
    [SerializeField] public int armor;
    [SerializeField] public int damage;
    [SerializeField] public float shootFrequency;
    [SerializeField] public float bulletSpeed;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] public int Credits;
    [SerializeField]
    private RectTransform lifeBar;

    private IEnumerator shootingEnumerator;
    private Mine mine;
    private float initialWidth;
    private float initialHealth;

    private void OnDestroy()
    {
        if(mine != null)
        {
            RemoveUnit();
        }
        if (shootingEnumerator != null)
        {
            StopCoroutine(shootingEnumerator);
        }
    }
    private void Start()
    {

        initialHealth = health;
        initialWidth = lifeBar.sizeDelta.x;
    }


    public void TakeDamage(int incomingDamage)
    {
        health = health - Mathf.Max(0, incomingDamage - armor);
        var ratio = Mathf.InverseLerp(0, initialHealth, health);
        lifeBar.sizeDelta = new Vector2(Mathf.Lerp(0, initialWidth, ratio), lifeBar.sizeDelta.y);
        if (health <= 0)
        {
            GameObject.Destroy(this.gameObject);
        }
    }

    public void StartShooting()
    {
        if(shootingEnumerator != null)
        {
            return;
        }
        shootingEnumerator = ShootOverTime();
        StartCoroutine(shootingEnumerator);
    }

    public void StopShooting()
    {
        StopCoroutine(shootingEnumerator);
        shootingEnumerator = null;
    }


    IEnumerator ShootOverTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(shootFrequency);
            StartCoroutine(Shoot());
        }
    }


    IEnumerator Shoot()
    {
        GameObject bulletInstance = GameObject.Instantiate(bulletPrefab);
        bulletInstance.transform.position = this.transform.position;
        bulletInstance.GetComponentInChildren<Bullet>().Damage = damage;
        bulletInstance.transform
            .DOMove(this.transform.right * 25, bulletSpeed)
            .SetRelative(true)
            .SetSpeedBased();
        yield return new WaitForSeconds(8f / bulletSpeed);
        GameObject.Destroy(bulletInstance);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(LayerMask.NameToLayer("Mine") == collision.transform.gameObject.layer)
        {
            mine = collision.transform.GetComponent<Mine>();
            mine.AddUnit(this.gameObject.layer == LayerMask.NameToLayer("Unit"));
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (LayerMask.NameToLayer("Mine") == collision.transform.gameObject.layer)
        {
            RemoveUnit();
        }

    }

    private void RemoveUnit()
    {
        mine.RemoveUnit(this.gameObject.layer == LayerMask.NameToLayer("Unit"));
        mine = null;
    }
    
}
