﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int Money;

    [SerializeField] private TextMeshProUGUI UIMoney;
    [SerializeField] private float updateTime = 1;
    [SerializeField] private int goldPerUpdate = 1;

    private void Start()
    {
        StartCoroutine(PassiveMoneyGain());
    }

    public IEnumerator PassiveMoneyGain()
    {
        while (this.isActiveAndEnabled)
        {
            yield return new WaitForSeconds(updateTime);
            Money += goldPerUpdate;
            UpdateUI();
        }
    }

    public void UpdateUI()
    {
        UIMoney.text = Money.ToString();
    }
}
